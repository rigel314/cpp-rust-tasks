use std::ffi;

extern "C" {
    fn cpplog(fmt: *const ffi::c_char, ...);
}

fn rust_log(fmt: String) {
    let fmt = ffi::CString::new(fmt).expect("CString::new failed");
    unsafe {
        cpplog(fmt.as_ptr());
    }
}

pub struct Rust {
    path: String,
}

impl Rust {
    pub fn new(path: String) -> Self {
        Rust { path }
    }
    pub fn init(&self) -> i32 {
        rust_log("rust::init[rs]\n".to_string());
	_ = self.path;
        0
    }
    pub fn step(&self) -> i32 {
        rust_log("rust::step[rs]\n".to_string());
        0
    }
}

#[no_mangle]
pub extern "C" fn rust_construct(path: *const ffi::c_char) -> *const ffi::c_void {
    let cstr = unsafe { ffi::CStr::from_ptr(path) };
    let path = String::from_utf8_lossy(cstr.to_bytes()).to_string();

    &mut Rust::new(path) as *mut Rust as *mut ffi::c_void
}

#[no_mangle]
pub extern "C" fn rust_init(ptr: *mut ffi::c_void) -> ffi::c_int {
    let result = std::panic::catch_unwind(|| {
        let r = unsafe { &*(ptr as *mut Rust) };
        r.init() as ffi::c_int
    });
    if result.is_err() {
        eprintln!("error: rust panicked");
        return 1;
    }
    result.unwrap()
}

#[no_mangle]
pub extern "C" fn rust_step(ptr: *mut ffi::c_void) -> ffi::c_int {
    let result = std::panic::catch_unwind(|| {
        let r = unsafe { &*(ptr as *mut Rust) };
        r.step() as ffi::c_int
    });
    if result.is_err() {
        eprintln!("error: rust panicked");
        return 1;
    }
    result.unwrap()
}
