#ifndef _core_h
#define _core_h

#include <vector>

class core
{
public:
    virtual int init() = 0;
    virtual int step() = 0;
};

class runner
{
public:
    runner();
    std::vector<core *> tasks;
    int run();
};

#endif // _core_h

extern "C" void cpplog(const char* fmt, ...);
