#include <stdio.h>

#include "rust.h"

extern "C" void *rust_construct(const char *path);
extern "C" int rust_init(void *handle);
extern "C" int rust_step(void *handle);

rust::rust(const char *path)
{
    handle = rust_construct(path);
}

int rust::init()
{
    cpplog("rust::init\n");
    // return ((rust::Rust*)(handle))->init();
    return rust_init(handle);
}
int rust::step()
{
    cpplog("rust::step\n");
    return rust_step(handle);
}
