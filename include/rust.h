#ifndef _rust_h
#define _rust_h

#include "core.h"

class rust : public core
{
public:
    rust(const char *path);
    virtual int init();
    virtual int step();

private:
    void *handle;
  //public:
  //class Rust{
  // public:
  //    int init();
  //};
};

#endif // _rust_h
