#include "exec.h"
#include "normal.h"
#include "rust.h"

int initTasks(runner *r)
{
    r->tasks.push_back(new normal());
    r->tasks.push_back(new rust("/blar"));

    return 0;
}
