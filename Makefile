dyobjects := libraries/core/core.so libraries/exec/exec.so libraries/normal/normal.so libraries/rust/rust.so libraries/rust/target/release/librust.so libraries/rust/target/release/libstd-8389830094602f5a.so
headers := $(wildcard include/*.h)

all: main Makefile

main: executables/main/main.o $(dyobjects) $(headers) Makefile
	g++ -o main -Wl,--start-group executables/main/main.o $(dyobjects)

%.so: %.o $(headers) Makefile
	g++ -fPIC -shared -o "$@" "$<"

%.o: %.cpp $(headers) Makefile
	g++ -fPIC -Ofast -Wall -Wextra -Iinclude -o "$@" -c "$<"

libraries/rust/target/release/librust.so: libraries/rust/src/lib.rs libraries/rust/Cargo.toml Makefile
	cd libraries/rust && cargo build -r

run: main Makefile
	LD_LIBRARY_PATH=libraries/rust/target/release ./main

check: Makefile
	cd libraries/rust && cargo clippy

objdump: libraries/rust/target/release/librust.so Makefile
	objdump -TC libraries/rust/target/release/librust.so | grep \\.text

clean:
	-rm */*/*.o
	-rm */*/*.so

.PHONY: clean all run check
.PRECIOUS: %.o
