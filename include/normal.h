#ifndef _normal_h
#define _normal_h

#include "core.h"

class normal : public core
{
    virtual int init();
    virtual int step();
};

#endif // _normal_h
