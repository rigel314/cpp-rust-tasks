#include "core.h"
#include "exec.h"

int main()
{
    runner r;

    initTasks(&r);

    return r.run();
}
