#include <unistd.h>
#include <stdarg.h>
#include <stdio.h>

#include "core.h"

runner::runner() {}

int runner::run()
{
    for (auto task : tasks)
    {
        task->init();
    }

    while (1)
    {
        usleep(1 * 1000 * 1000);
        for (auto task : tasks)
        {
            task->step();
        }
    }
}

extern "C" void cpplog(const char *fmt, ...)
{
    va_list args;
    va_start(args, fmt);
    vprintf(fmt, args);
    va_end(args);
}
